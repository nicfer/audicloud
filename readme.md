# Audicloud 

It's a tool for parsing nextcloud's audit logs.

## Usage

```
usage: parser.py [-h] -f FILE (-l LOGIN | -a IPADRESS) [-ofa]

Nextcloud log parser

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  file to parse
  -l LOGIN, --login LOGIN
                        login use to list the activity
  -a IPADRESS, --ipAdress IPADRESS
                        returns the logins linked to the IP address
  -ofa, --onlyFilesActivities
                        show only files activities

```

## Licence
[AGPL](https://www.gnu.org/licenses/lgpl-3.0.html)