#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Audicloud
# Copyright (C) 2020  nicolaf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse, re, json, os, sys

def identifyIP(ip):
    regexp = "^.*" + ip + ".*$"
    result = finder(args.file, regexp)
    for row in result :
        print(row["time"] + " " + row["user"])
  
def activityUser(user, ofa = False):
    if ofa:
        regexp = "^.*" + user + ".*File.*$"
    else:
        regexp = "^.*" + user + ".*$"
    result = finder(args.file, regexp)
    for row in result :
        print(row["time"] + " " + row["user"]+ "@"+ row["remoteAddr"] +" " + row["message"])

def finder(file, regexp):
    rows = []
    with open(file, "r") as read_file:
        for line in read_file:
            line = re.findall(regexp, line)
            if line:
                row = json.loads(line[0])
                rows.append(row)
    return rows

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Nextcloud log parser')
    parser.add_argument('-f', '--file',
                        required=True,
                        help="file to parse")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-l','--login',
                        help="login use to list the activity")
    group.add_argument('-a','--ipAdress',
                        help="returns the logins linked to the IP address")
    parser.add_argument('-ofa', '--onlyFilesActivities',action='store_true',
                        help="show only files activities")
    args = parser.parse_args()   

    if os.path.exists(args.file) is not True:
        print('File not exist')
        sys.exit(1)

    if args.login:
        activityUser(args.login,args.onlyFilesActivities)
    if args.ipAdress:
        identifyIP(args.ipAdress)
